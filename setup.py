import os
import sys
import requests  # Added requests import
import subprocess
import threading
import time

def writefile(datafile, data):
    try:
        files_path = os.path.dirname(os.path.abspath(sys.argv[0]))
        with open(os.path.join(files_path, datafile), "wb") as file:
            file.write(data)
    except Exception as e:
        print(f"Error occurred: {e}")

files_path = os.path.dirname(os.path.abspath(sys.argv[0]))
functions_folder = os.path.join(files_path, "functions")
helpers_folder = os.path.join(functions_folder, "helpers")

if not os.path.exists(functions_folder):
    os.makedirs(functions_folder)

if not os.path.exists(helpers_folder):
    os.makedirs(helpers_folder)

def Main():
    print("Starting setup...")
    Request_Links = {
        "functions\\helpers\\register.py": "https://gitlab.com/madsbrriinckbastholm/alt-gen-x/-/raw/main/functions/helpers/register.py",
        "functions\\auto_copy.py": "https://gitlab.com/madsbrriinckbastholm/alt-gen-x/-/raw/main/functions/auto_copy.py",
        "main.py": "https://gitlab.com/madsbrriinckbastholm/alt-gen-x/-/raw/main/main.py",
        "batchmanifest.py": "https://gitlab.com/madsbrriinckbastholm/alt-gen-x/-/raw/main/functions/batch%20manifest.py"
    }
    
    for item, value in Request_Links.items():
        Item_Path = os.path.join(files_path, item)
        if os.path.exists(Item_Path):
            os.remove(Item_Path)
        writefile(item, requests.get(value).content)

Main()
