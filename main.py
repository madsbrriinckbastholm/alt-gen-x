from functions.helpers.register import main, readfile
from functions.auto_copy import main_copy
import threading, time, os, subprocess, sys
from roblox_auth import multi_roblox

def FAFAFAAF():
    timeout = 60
    files_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    batchmanifest = os.path.join(files_path, "batchmanifest.py")

    start_time = time.time()
    while not os.path.exists(batchmanifest):
        if time.time() - start_time > timeout:
            print("Timeout reached. File not found.")
            break
        time.sleep(1)

    if os.path.exists(batchmanifest):
        subprocess.call([sys.executable, "batchmanifest.py"])

data = readfile("settings.json")

auto_copy = data["AUTO_COPY_CODES"]
MULTI_ROBLOX_THREADING = data["MULTI_ROBLOX_THREADING"]

if MULTI_ROBLOX_THREADING:
    multi_roblox()

if auto_copy:
    thread_one = threading.Thread(target=main_copy)
    thread_one.start()

thread_three = threading.Thread(target=FAFAFAAF)
thread_three.start()

while True:
    thread_two = threading.Thread(target=main)
    thread_two.start()
    thread_two.join()
